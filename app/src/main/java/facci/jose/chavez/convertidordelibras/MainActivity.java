package facci.jose.chavez.convertidordelibras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

EditText TextDatos;
Button buttonConvertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextDatos = (EditText)findViewById(R.id.editTextPasarParametro);
        buttonConvertir = (Button)findViewById(R.id.Convertir);

        buttonConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double multiplicador = 453.593;
                double resultado = 0.0;
                double valorLibras = Double.parseDouble(TextDatos.getText().toString());

                resultado = valorLibras * multiplicador;


                Intent intent = new Intent( MainActivity.this, ConvertidodeLibas.class);
                startActivity(intent);
            }
        });

        buttonConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainActivity.this,ConvertidodeLibas.class);
                Bundle bundle = new Bundle();

                bundle.putString("dato",TextDatos.getText().toString());

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

}

package facci.jose.chavez.convertidordelibras;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ConvertidodeLibas extends AppCompatActivity {

    TextView Libras;

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("4a","Conversion Finalizada");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidode_libas);


        Libras = (TextView)findViewById(R.id.lblParametro);

        Bundle bundle = this.getIntent().getExtras();
        Libras.setText(bundle.getString("dato"));

    }
}
